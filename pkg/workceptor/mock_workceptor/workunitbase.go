// Code generated by MockGen. DO NOT EDIT.
// Source: pkg/workceptor/workunitbase.go
//
// Generated by this command:
//
//	mockgen -source=pkg/workceptor/workunitbase.go -destination=pkg/workceptor/mock_workceptor/workunitbase.go
//

// Package mock_workceptor is a generated GoMock package.
package mock_workceptor

import (
	reflect "reflect"

	fsnotify "github.com/fsnotify/fsnotify"
	gomock "go.uber.org/mock/gomock"
)

// MockWatcherWrapper is a mock of WatcherWrapper interface.
type MockWatcherWrapper struct {
	ctrl     *gomock.Controller
	recorder *MockWatcherWrapperMockRecorder
	isgomock struct{}
}

// MockWatcherWrapperMockRecorder is the mock recorder for MockWatcherWrapper.
type MockWatcherWrapperMockRecorder struct {
	mock *MockWatcherWrapper
}

// NewMockWatcherWrapper creates a new mock instance.
func NewMockWatcherWrapper(ctrl *gomock.Controller) *MockWatcherWrapper {
	mock := &MockWatcherWrapper{ctrl: ctrl}
	mock.recorder = &MockWatcherWrapperMockRecorder{mock}
	return mock
}

// EXPECT returns an object that allows the caller to indicate expected use.
func (m *MockWatcherWrapper) EXPECT() *MockWatcherWrapperMockRecorder {
	return m.recorder
}

// Add mocks base method.
func (m *MockWatcherWrapper) Add(name string) error {
	m.ctrl.T.Helper()
	ret := m.ctrl.Call(m, "Add", name)
	ret0, _ := ret[0].(error)
	return ret0
}

// Add indicates an expected call of Add.
func (mr *MockWatcherWrapperMockRecorder) Add(name any) *gomock.Call {
	mr.mock.ctrl.T.Helper()
	return mr.mock.ctrl.RecordCallWithMethodType(mr.mock, "Add", reflect.TypeOf((*MockWatcherWrapper)(nil).Add), name)
}

// Close mocks base method.
func (m *MockWatcherWrapper) Close() error {
	m.ctrl.T.Helper()
	ret := m.ctrl.Call(m, "Close")
	ret0, _ := ret[0].(error)
	return ret0
}

// Close indicates an expected call of Close.
func (mr *MockWatcherWrapperMockRecorder) Close() *gomock.Call {
	mr.mock.ctrl.T.Helper()
	return mr.mock.ctrl.RecordCallWithMethodType(mr.mock, "Close", reflect.TypeOf((*MockWatcherWrapper)(nil).Close))
}

// ErrorChannel mocks base method.
func (m *MockWatcherWrapper) ErrorChannel() chan error {
	m.ctrl.T.Helper()
	ret := m.ctrl.Call(m, "ErrorChannel")
	ret0, _ := ret[0].(chan error)
	return ret0
}

// ErrorChannel indicates an expected call of ErrorChannel.
func (mr *MockWatcherWrapperMockRecorder) ErrorChannel() *gomock.Call {
	mr.mock.ctrl.T.Helper()
	return mr.mock.ctrl.RecordCallWithMethodType(mr.mock, "ErrorChannel", reflect.TypeOf((*MockWatcherWrapper)(nil).ErrorChannel))
}

// EventChannel mocks base method.
func (m *MockWatcherWrapper) EventChannel() chan fsnotify.Event {
	m.ctrl.T.Helper()
	ret := m.ctrl.Call(m, "EventChannel")
	ret0, _ := ret[0].(chan fsnotify.Event)
	return ret0
}

// EventChannel indicates an expected call of EventChannel.
func (mr *MockWatcherWrapperMockRecorder) EventChannel() *gomock.Call {
	mr.mock.ctrl.T.Helper()
	return mr.mock.ctrl.RecordCallWithMethodType(mr.mock, "EventChannel", reflect.TypeOf((*MockWatcherWrapper)(nil).EventChannel))
}
